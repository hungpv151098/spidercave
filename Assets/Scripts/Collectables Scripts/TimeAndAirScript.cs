﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeAndAirScript : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Debug.Log(gameObject.name);
            if (gameObject.CompareTag("Air"))
            {
                var airTimer = GamePlayController.instance.GetComponent<AirTimer>().air;
                airTimer += 15f;
                GamePlayController.instance.GetComponent<AirTimer>().air = airTimer;
                Debug.Log(airTimer);
            }
            else 
            {
                var levelTimer = GamePlayController.instance.GetComponent<LevelTimer>().timer;
                levelTimer += 15f;
                GamePlayController.instance.GetComponent<LevelTimer>().timer = levelTimer;
                Debug.Log(levelTimer);
            }
            Destroy(gameObject);
        }
    }
}
