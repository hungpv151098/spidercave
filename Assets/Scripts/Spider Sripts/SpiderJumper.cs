﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderJumper : MonoBehaviour
{
    public float forceY = 400f;

    private Rigidbody2D rb2D;
    private Animator anim;

    void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(3);

        forceY = Random.Range(250f, 500f);
        rb2D.AddForce(new Vector2(0, forceY));
        anim.SetBool("Attack", true);

        yield return new WaitForSeconds(.7f);
        StartCoroutine(Attack());
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            GameObject.Find("GameplayController").GetComponent<GamePlayController>().PlayerDied();
            Destroy(col.gameObject);
        }
        if (col.CompareTag("Ground"))
        {
            anim.SetBool("Attack", false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
