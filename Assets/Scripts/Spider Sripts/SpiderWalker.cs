﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderWalker : MonoBehaviour
{
    [SerializeField]
    private Transform startPos, endPos;

    private bool col;

    public float speed = 1f;
    private Rigidbody2D rb2D;

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
        ChangeDirection();
        
    }

    void ChangeDirection()
    {
        col = Physics2D.Linecast(startPos.position, endPos.position,1 << LayerMask.NameToLayer("HighGround"));

        Debug.DrawLine(startPos.position, endPos.position, Color.green);

        if(!col)
        {
            Vector3 temp = transform.localScale;
            if(temp.x == 1f)
            {
                temp.x = -1f;
            }
            else
            {
                temp.x = 1f;
            }
            transform.localScale = temp;
        }


    }

    void Move()
    {
        rb2D.velocity = new Vector2(transform.localScale.x, 0) * speed;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            GameObject.Find("GameplayController").GetComponent<GamePlayController>().PlayerDied();
            Destroy(col.gameObject);
        }
    }
}
