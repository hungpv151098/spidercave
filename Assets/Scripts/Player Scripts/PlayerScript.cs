﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerScript : MonoBehaviour
{

    public ParticleSystem dustPSRunning;
    public ParticleSystem dustPSJumping;
    public static PlayerScript instance;

    [Header("Horizontal Movement")]
    public float moveSpeed = 10f;
    public Vector2 direction;
    private bool facingRight = true;

    [Header("Vertical Movement")]
    public float jumpForce = 15f;
    public float jumpDelay = 0.25f;
    private float jumpTimer;
    public GameObject characterHolder;
    public int jumpCount = 2;
    private int jumps;

    [Header("Components")]
    private Rigidbody2D rb2D;
    private Animator anim;
    //public GameObject characterHolder;

    [Header("Physics")]
    public float maxSpeed = 7f;
    public float linearDrag = 4f;
    public float gravity = 1f;
    public float fallMultiplier = 5f;

    [Header("Collision")]
    public Vector3 colliderOffset;
    public bool isGrounded = false;
    public bool isHighGround = false;
    public bool canDoubleJump = false;
  
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        rb2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
  
    void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            jumpTimer = Time.time + jumpDelay;
        }

        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));    
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        MoveCharacter(direction.x);
        if(jumpTimer > Time.time && (isGrounded || isHighGround))
        {
            Jump();
            canDoubleJump = true;
        }
        else
        {
            if(jumpTimer > Time.time && canDoubleJump)
            {
                Jump();
                canDoubleJump = false;
            }
        }
        modifyPhysics();

    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            isGrounded = true;
            isHighGround = false;
        }
        else if (col.gameObject.layer == LayerMask.NameToLayer("HighGround"))
        {
            isGrounded = false;
            isHighGround = true;
        }


    }

    private void MoveCharacter(float horizontal)
    {
        rb2D.AddForce(Vector2.right * horizontal * moveSpeed);
        if ((horizontal > 0 && !facingRight) || (horizontal<0 && facingRight)){
            Flip();
        }
        if (Mathf.Abs(rb2D.velocity.x) > maxSpeed)
        {
            rb2D.velocity = new Vector2(Mathf.Sign(rb2D.velocity.x) * maxSpeed, rb2D.velocity.y);
        }
        anim.SetFloat("horizontal", Mathf.Abs(rb2D.velocity.x));
        anim.SetFloat("vertical", rb2D.velocity.y);
    }

    void Flip()
    {
        CreateDustWhenRunning();
        facingRight = !facingRight;
        transform.rotation = Quaternion.Euler(0, facingRight ? 0 : 180, 0);
    }

    void Jump()
    {
        CreateDustWhenJumping();
        isGrounded = false;
        isHighGround = false;
        rb2D.velocity = new Vector2(rb2D.velocity.x, 0);
        rb2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        jumpTimer = 0;
        
    }


    void modifyPhysics()
    {
        bool changingDirections = (direction.x > 0 && rb2D.velocity.x < 0) || (direction.x < 0 && rb2D.velocity.x > 0);
        if (isGrounded)
        {
            if (Mathf.Abs(direction.x) < 0.4f || changingDirections)
            {
                rb2D.drag = linearDrag;
            }
            else
            {
                rb2D.drag = 0f;
            }
            rb2D.gravityScale = 0f;
        }
        else
        {
            rb2D.gravityScale = gravity;
            rb2D.drag = linearDrag * 0.15f;
            if (rb2D.velocity.y < 0)
            {
                rb2D.gravityScale = gravity * fallMultiplier;
            }else if(rb2D.velocity.y>0 && !Input.GetButton("Jump"))
            {
                rb2D.gravityScale = gravity * (fallMultiplier / 2);
            }
        }
    }
    //IEnumerator JumpSqueeze(float xSqueeze, float ySqueeze, float seconds)
    //{
    //    Vector3 originalSize = Vector3.one;
    //    Vector3 newSize = new Vector3(xSqueeze, ySqueeze, originalSize.z);
    //    float t = 0f;
    //    while (t <= 1.0)
    //    {
    //        t += Time.deltaTime / seconds;
    //        characterHolder.transform.localScale = Vector3.Lerp(originalSize, newSize, t);
    //        yield return null;
    //    }
    //    t = 0f;
    //    while (t <= 1.0)
    //    {
    //        t += Time.deltaTime / seconds;
    //        characterHolder.transform.localScale = Vector3.Lerp(newSize, originalSize, t);
    //        yield return null;
    //    }

    //}
    void CreateDustWhenRunning()
    {
        dustPSRunning.Play();
        Instantiate(dustPSRunning, transform.position, dustPSRunning.transform.rotation);
    }
    void CreateDustWhenJumping()
    {
        dustPSRunning.Play();
        
    }




    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

    }
}
