﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public static Door instance;

    private Animator anim;
    private BoxCollider2D bc2D;

    [HideInInspector]
    public int collectablesCount;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        anim = GetComponent<Animator>();
        bc2D = GetComponent<BoxCollider2D>();
        
    }

    public void DecrementCollectables()
    {
        collectablesCount--;
        StartCoroutine(OpenDoor());
       
    }

    IEnumerator OpenDoor()
    {

        yield return new WaitUntil(()=>collectablesCount == 0);
        bc2D.isTrigger = true;  
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            GamePlayController.instance.GetComponent<GamePlayController>().PlayerDied();
        }   
    }

    
}
